<?php

/**
 * @file
 * Preprocessors and helper functions to make theming easier.
 */

use Drupal\Core\Template\Attribute;
use Drupal\tailwind_grid\TailwindGrid;

/**
 * Prepares variables for views grid templates.
 *
 * Default template: views-tailwind-grid.html.twig.
 *
 * @param array $vars
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_views_tailwind_grid(array &$vars) {
  $view = $vars['view'];
  $vars['id'] = TailwindGrid::getUniqueId($view);
  $options = $view->style_plugin->options;
  $vars['row_attributes'] = new Attribute();
  foreach (TailwindGrid::getBreakpoints() as $breakpoint) {
    if ($options["$breakpoint"] == 'none') {
      continue;
    }
    $vars['row_attributes']->addClass($options["$breakpoint"]);
  }
  $vars['options'] = $options;
}
