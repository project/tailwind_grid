<?php

namespace Drupal\tailwind_grid\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\tailwind_grid\TailwindGrid;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * Style plugin to render each item in an ordered or unordered list.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "views_tailwind_grid",
 *   title = @Translation("Tailwind Grid"),
 *   help = @Translation("Displays rows in a Tailwind Grid layout"),
 *   theme = "views_tailwind_grid",
 *   theme_file = "../grid_view_tailwind.theme.inc",
 *   display_types = {"normal"}
 * )
 */
class ViewsTailwindGrid extends StylePluginBase {
  /**
   * Overrides \Drupal\views\Plugin\views\style\StylePluginBase::usesRowPlugin.
   *
   * @var bool
   */
  protected $usesRowPlugin = TRUE;

  /**
   * Overrides \Drupal\views\Plugin\views\style\StylePluginBase::usesRowClass.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Definition.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    foreach (TailwindGrid::getBreakpoints() as $breakpoint) {
      $breakpoint_option = "$breakpoint";
      $options[$breakpoint_option] = ['default' => 'none'];
    }

    $options['row_class_default'] = ['default' => TRUE];
    $options['default'] = ['default' => ''];
    $options['info'] = ['default' => []];
    $options['override'] = ['default' => TRUE];
    $options['sticky'] = ['default' => FALSE];
    $options['order'] = ['default' => 'asc'];
    $options['caption'] = ['default' => ''];
    $options['summary'] = ['default' => ''];
    $options['description'] = ['default' => ''];
    return $options;
  }



  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    foreach (TailwindGrid::getBreakpoints() as $breakpoint) {
      $breakpoint_option = "$breakpoint";
      $form[$breakpoint_option] = [
        '#type' => 'select',
        '#title' => $this->t("Column width of items at '$breakpoint' breakpoint"),
        '#default_value' => $this->options[$breakpoint_option] ?? NULL,
        '#description' => $this->t("Set the number of columns each item should take up at the '$breakpoint' breakpoint and higher."),
        '#options' => [
          'none' => 'None (or inherit from previous)',
        ],
      ];
      foreach ([1, 2, 3, 4, 6, 12] as $width) {
        $form[$breakpoint_option]['#options']["$breakpoint:col-span-$width"] = $this->formatPlural(12 / $width, '@count column per row', '@count columns per row', ['@width' => $width]);
      }
    }
  }

}
