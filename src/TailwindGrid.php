<?php

namespace Drupal\tailwind_grid;

use Drupal\Component\Utility\Html;
use Drupal\views\ViewExecutable;

/**
 * The primary class for the Views Tailwind module.
 *
 * Provides many helper methods.
 *
 * @ingroup utility
 */
class TailwindGrid {

  /**
   * Returns the theme hook definition information.
   */
  public static function getThemeHooks() {
    $hooks['views_tailwind_grid'] = [
      'preprocess functions' => [
        'template_preprocess_views_tailwind_grid',
      ],
      'file' => 'grid_view_tailwind.theme.inc',
    ];

    return $hooks;
  }

  /**
   * Return an array of breakpoint names.
   */

  /**
   * Return an array of breakpoint names.
   */
  public static function getBreakpoints() {
    $config = \Drupal::config('tailwind_grid.settings');
    $breakpoints = $config->get('breakpoints');

    if (is_string($breakpoints)) {
      $breakpoints = array_filter(array_map('trim', explode("\n", $breakpoints)));
    }

    if (!is_array($breakpoints)) {
      $breakpoints = [];
    }

    return $breakpoints;
  }

  /**
   * Get unique element id.
   *
   * @param \Drupal\views\ViewExecutable $view
   *   A ViewExecutable object.
   *
   * @return string
   *   A unique id for an HTML element.
   */
  public static function getUniqueId(ViewExecutable $view) {
    $id = $view->storage->id() . '-' . $view->current_display;
    return Html::getUniqueId('views-tailwind-' . $id);
  }

}
