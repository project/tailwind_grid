<?php

namespace Drupal\tailwind_grid\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form for Tailwind Grid breakpoints.
 */
class BreakpointsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tailwind_grid_breakpoints_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tailwind_grid.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tailwind_grid.settings');

    $breakpoints = $config->get('breakpoints');

    if (empty($breakpoints)) {
      $breakpoints = "sm\nmd\nlg\nxl\n2xl";
    }
    elseif (is_array($breakpoints)) {
      $breakpoints = implode("\n", $breakpoints);
    }

    $form['breakpoints'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Breakpoints'),
      '#description' => $this->t('Enter the breakpoints, one per line.'),
      '#default_value' => $breakpoints,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $breakpoints = array_filter(array_map('trim', explode("\n", $form_state->getValue('breakpoints'))));

    $this->config('tailwind_grid.settings')
      ->set('breakpoints', $breakpoints)
      ->save();
  }

}
