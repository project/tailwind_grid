CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Using Views
 * Organization
 * Maintainers

INTRODUCTION
------------

The Views Tailwind module adds styles to Views to output the results of a view
as several common Tailwind components.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/tailwind_grid

REQUIREMENTS
------------

This module requires the following themes/modules:

 * Views (Core)

INSTALLATION
------------

 * Install the Tailwind Grid module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Go to Configure and add the breakpoints you have inside your tailwind.config.

USING VIEWS
-----------

    1. First, add the desired style type. You can choose from a variety of Tailwind CSS grid styles based on your needs.
    2. After selecting a style, you can customize the settings for that specific component to fit your design requirements.

ORGANIZATION
------------

  This project is maintained by Sprintive - https://www.drupal.org/sprintive

  For more information, please visit our website at https://sprintive.com/.

MAINTAINERS
-----------

 * M. Abdulqader (m.abdulqader) - https://www.drupal.org/u/mabdulqader
 * Osama Nuserat (osama.nuserat) - https://www.drupal.org/u/osama-nuserat
